##    CS 101
##    Program 7
##    Jackie Batson
##    jbvx8@umkc.edu
##    Created: 4/19/15
##    Due: 4/26/15
##
##    Problem: Given 3 movie .csv files, be able to return the top 10 movies in two different genres
##    provided by a user, in order of rating.  Also provide all the movies a user-given actor has
##    appeared in, in order of rating.
##
##    Algorithm:
##    1. Import csv module
##    2. Build three dictionaries (if the needed files are not found, quit):
##            1)An actor dictionary with sets as values. Open the actor csv file and read it.
##                    a)For each row in the csv file:
##                            i) actor key = row[actor column]
##                            ii) if actor key in actor dictionary:
##                                    value = actor dictionary[actor key]
##                                    add row[movie column] to value (which is a set, initialized below)
##                            iii) else actor dictionary[actor key] = set(row[movie column])
##            2)A genre dictionary from the genre csv file, built similarly to the actor dictionary,
##              with the genre as the key and movies in that genre added to a value set for each genre.
##            3)A movie dictionary. Open the movie cvs file and read it.
##                    a)For each row in the csv file:
##                            i)movie dictionary[row[movie column]] = float(row[rating column])
##                    b)This will result in a simple dictionary with the movie as key and its rating
##                      as the value.
##            4)Close all files after each dictionary is built.
##    3. Make a list of valid genres from the genre dictionary keys
##    4. Make a list of valid actors from the actor dictionary keys
##    5. Prompt the user to enter whether they'd like to find the top movies in two genres, the movies
##       for a given actor, or quit. Check to make sure the entry is valid and continue prompting until
##       a valid entry is made.
##    6. If the user wants to see two genres:
##            1)Prompt the user to enter a genre. Using the valid genre list, check whether the entry is
##              valid and continue prompting until a valid choice is made.
##            2)Repeat above step for a second genre.
##            3)To find the intersection of the two genres, assign the value of the genre dictionary for
##              the given key to a new set variable. Do the same for the second genre.
##            4)Find the intersection of the two sets and return a new set.
##            5)To rank the movies, build a 2D list containing lists of each movie and its rating. For
##              each item in the intersection set:
##                    a)assign to a value variable the value for the movie item in the movie dictionary
##                      (will be the rating)
##                    b)append a value list with a list of [value, item]
##                    c)sort and reverse the list to get the movies of both genres in rating order from
##                      high to low.
##            6)Get a slice of the first ten values of the value list to get the top 10 movies.
##            7)Print the results.
##    7. If the user wants to see all movies for a given actor:
##            1)Prompt the user to enter an actor exactly. Using the valid actor list, check whether the
##              entry is valid and continue prompting until a valid choice is made.
##            2)Get the value set from the actor dictionary, using the actor name as key and returning
##              the set of all movies the actor is in.
##            3)Rank the movies by building a value list similar to step 6.5.
##            4)Print the results 
##    8. Return to step 5 until the user enters a "Q" to quit.


import csv
import sys  #used to exit the program if file not found

#used to open the movie, genre, and actor files.
def open_file(filename):
    """Opens given files from the same directory that the program runs from. Displays an
       error and quits the program if the file is not found.
       :param filename: the string name of file to open.

       This function does not return a value, it just opens the file.
       """    
    try:
        file_handle = open(filename)
        return file_handle
    except FileNotFoundError:
        print(filename, "not found. Make sure it is in the same directory and re-try.")
        sys.exit(0) #quits the program


#used for actor and genre dict
def build_dict_set_values(filename, key_col, val_col):
    """Builds a dictionary from a csv file with sets for values.
       :param filename: string name of csv file containing data to make a dictionary from
       :param key_col: the number of the csv column (starting from 0) containing the keys
       :param val_col: the number of the csv column containing the values
       :return new_dict: a dictionary containing the values in a set
       """   
    file_open = open_file(filename)
    csv_file = csv.reader(file_open)
    new_dict = {}
    for row in csv_file:
        key = row[key_col]
        if key in new_dict:
            value = new_dict[key]   #assigns the value set to value if key already in dictionary
            value.add(row[val_col]) #adds the current value to the value set
        else:
            new_dict[key] = set(row[val_col]) #creates a new key:value set pair if entry not found
    file_open.close()
    return new_dict


#used for movie titles and rating
def build_dict_from_file(filename, key_col, val_col):
    """Builds a dictionary from csv file using floating points as values
       :param filename: string of the csv file name containing desired data
       :param key_col: the number of the csv column (starting from 0) containing the keys
       :param val_col: the number of the csv column containing the values
       :return new_dict: a dictionary containing the key:value pairs
       """    
    new_dict = {}
    file_open = open_file(filename)
    file_csv = csv.reader(file_open)
    
    for row in file_csv:
        new_dict[row[key_col]] = float(row[val_col])

    file_open.close()
    return new_dict


#used to get the user's menu choice, actor choice, and genre choices
def get_user_choice(prompt, valid_choices, error_text):
    """Displays a prompt asking the user for input, displays an error and loops back if input is
       invalid.
       :param prompt: string displayed to user asking for input
       :param valid_choices: a collection of valid inputs
       :param error_text: string displayed to the user if the input is invalid.
       :return user_choice: the valid input given by the user.

       To work properly, the valid choices must be in title format (first letters capitalized)
       """    
    user_choice = input(prompt).title()
    while user_choice not in valid_choices:
        print(error_text)
        user_choice = input(prompt).title()
    return user_choice


#used to find the intersection of movies in two genres
def intersect_dict_sets(key1, key2, set_dictionary):
    """Intersects two sets from a dictionary's values
       :param key1: key from dictionary to find first set
       :param key2: key from dictionary to find second set to intersect with
       :param set_dictionary: the dictionary to find the two keys with set values
       :return: the intersection of first and second set, as a new set
       """    
    set1 = set_dictionary[key1]
    set2 = set_dictionary[key2]
    return set1.intersection(set2)


#used to rank movies by rating for actor and interesected genre
def rank_items(collection, dictionary):
    """Builds a 2D list using a collection to find keys in a dictionary, then appending the list with
       a list of value, key, then sorting that list and ordering with highest value first.
       :param collection: a collection of possible keys
       :param dictionary: the dictionary to look up keys from the collection in and get their values
       :return value_list: a reverse sorted 2D list
       """    
    value_list = []
    for item in collection:
        if item in dictionary:
            value = dictionary[item]  #assigns the value for a key in the collection to value variable
            value_list.append([value, item]) #appends a list of the value and key
    value_list.sort(reverse = True)  #sorts the list in order of highest value to lowest
    return value_list


#used to get top 10 movies by rating
def get_top_10(sorted_list):
    """Gets the first ten values of a sorted list
       :param sorted_list: the sorted list with desired ten values first
       :return: a new list of the first ten values from the sorted list
       """
    return sorted_list[:10]


def print_results(sorted_list, index0, index1):
    """Prints out a formatted display of values from a 2D list
       :param sorted_list: a 2D list arranged in the desired order to print
       :param index0: the index position in the sublist to print first
       :param index1: the index position in the sublist to print second

       This function does not return a value; it prints out values.
       """
    print("{:80}{:>10}".format("Title", "Rating"))
    print("="*90)
    for item in sorted_list:
        print("{:80}{:10}".format(item[index0], item[index1]))
        
    
#some valid choices and display prompts and errors
valid_menu = ["1", "2", "Q"]

menu_prompt = """Local Movie Database

1)Find the highest rated movies for two genres
2)Show all movies by rating for a chosen actor
Q)Quit

=> """

genre_prompt1 = "Enter the name of a movie genre to search: "
genre_prompt2 = "Enter the name of another genre to find movies of both genres: " 
actor_prompt = "Enter the name of an actor or actress to search. Name must exactly match Last, First: "
menu_error = "Your choice must be 1, 2, or Q."
genre_error = "That genre was not found."
actor_error = "That actor was not found."


if __name__ == "__main__":

    print("Reading files...")  #the next steps may take a second, so display some text to user
    #build the three dictionaries
    actor_dict = build_dict_set_values("actor_movie.csv", 0, 1)
    genre_dict = build_dict_set_values("genre.csv", 1, 0)
    movie_dict = build_dict_from_file("movies.csv", 0, 2)

    #get valid user entries from the generated dictionaries' keys
    valid_genre = genre_dict.keys()
    valid_actors = actor_dict.keys()

    while True:  #while loop comes later, because the above steps only need to be executed once
        menu_choice = get_user_choice(menu_prompt, valid_menu, menu_error)
        if menu_choice == "1":  #user wants to view movies in two genres
            genre_choice1 = get_user_choice(genre_prompt1, valid_genre, genre_error)
            genre_choice2 = get_user_choice(genre_prompt2, valid_genre, genre_error)
            genre_intersection = intersect_dict_sets(genre_choice1, genre_choice2, genre_dict)
            genre_ranking = rank_items(genre_intersection, movie_dict)
            genre_top_ten = get_top_10(genre_ranking)
            print()
            print("The top 10 movies in the genres", genre_choice1, "and", genre_choice2, "are:")
            print_results(genre_top_ten, 1, 0)
            print()
        elif menu_choice == "2":  #user wants to view all movies for a given actor
            actor_choice = get_user_choice(actor_prompt, valid_actors, actor_error)
            actor_movies = actor_dict[actor_choice]
            actor_ranking = rank_items(actor_movies, movie_dict)
            print()
            print("The movies for", actor_choice, "are: ")      
            print_results(actor_ranking, 1, 0)
            print()
        else:  #user quits
            print("Thanks for using the Local Movie Database")
            break
    
    


        
